
#include <stdio.h>
#include <direct.h>
#include <string>
#include <map>
#include "pugixml.hpp"

std::map<uint32_t, std::string> HashMap;

void ProcessFileNode(pugi::xml_node_iterator file)
{
	pugi::xml_attribute validId = file->attribute("Id");
	pugi::xml_node validShortName = file->child("ShortName");

	if (!validId || !validShortName)
	{
		printf("WARNING: Invalid Node\n");
		return;
	}

	uint32_t hash = std::stoul(validId.value());
	std::string name = validShortName.child_value();

	auto it = HashMap.find(hash);
	if (it != HashMap.end() && it->second != name)
	{
		printf("WARNING: Hash collision for %u: %s <- %s\n", hash, it->second.c_str(), name.c_str());
	}

	HashMap.emplace(hash, name);
}

void BuildHashMap(pugi::xml_node soundbanksInfo)
{
	pugi::xml_node soundBanks = soundbanksInfo.child("SoundBanks");

	for (pugi::xml_node_iterator soundBank = soundBanks.begin(); soundBank != soundBanks.end(); ++soundBank)
	{
		pugi::xml_node includedEvents = soundBank->child("IncludedEvents");

		if (!includedEvents.empty())
		{
			for (pugi::xml_node_iterator event = includedEvents.begin(); event != includedEvents.end(); ++event)
			{
				pugi::xml_node referencedStreamedFiles = event->child("ReferencedStreamedFiles");

				if (!referencedStreamedFiles.empty())
				{
					for (pugi::xml_node_iterator file = referencedStreamedFiles.begin(); file != referencedStreamedFiles.end(); ++file)
					{
						ProcessFileNode(file);
					}
				}
				
				pugi::xml_node includedMemoryFiles = event->child("IncludedMemoryFiles");
				
				if (!includedMemoryFiles.empty())
				{
					for (pugi::xml_node_iterator file = includedMemoryFiles.begin(); file != includedMemoryFiles.end(); ++file)
					{
						ProcessFileNode(file);
					}
				}
			}
		}
	}
}

void RenameFiles()
{
	if (_mkdir("SFX") != 0)
	{
		//printf("WARNING: Could not create subfolder!");
	}
	
	for (std::pair<uint32_t, std::string> pair : HashMap)
	{
		std::string oldName = std::to_string(pair.first) + ".wav";
		std::string newName = pair.second;
		std::string dir;

		if (std::rename(oldName.c_str(), oldName.c_str()) != 0)
		{
			continue;
		}

		size_t slashPos = newName.rfind('\\');

		if (slashPos != std::string::npos)
		{
			dir = "SFX\\" + newName.substr(0, slashPos);
			slashPos = 0;
		}

		while (slashPos != std::string::npos)
		{
			std::string folder = dir.substr(0, dir.find('\\', slashPos + 1));
			
			if (_mkdir(folder.c_str()) != 0)
			{
				//printf("WARNING: Could not create subfolder!");
			}

			slashPos = dir.find('\\', slashPos + 1);
		}

		newName = "SFX\\" + newName;

		if (std::rename(oldName.c_str(), newName.c_str()) == 0)
		{
			continue;
		}
	}
}

void DisplayExitMessage()
{
	printf("Press Enter to exit...");
	int unused = 0;
	unused = getchar();
}

int main()
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file("SoundbanksInfo.xml");

	if (result)
	{
		printf("Generating hash map...\n");
		BuildHashMap(doc.document_element());
		
		printf("Renaming files...\n");
		RenameFiles();
	}
	else
	{
		printf("Error reading Soundbanks.xml - %s\n", result.description());
		DisplayExitMessage();
		return 0;
	}

	printf("All files finished\n");
	DisplayExitMessage();
	return 0;
}
